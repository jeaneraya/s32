let http = require('http');

let courses = [
	{
		"name": 'Javascript 101',
		"description": 'Introduction to Javascript',
	},
	{

		"name": 'HTML and CSS 101',
		"description": 'Introduction to HTML and CSS',
	}
];

let port = 4000

let server = http.createServer(function(request, response) {
	if(request.url == '/' && request.method == 'GET') {
		response.writeHead(200, {'Content-Type': 'application/json'})
		response.end('Welcome to Booking System.')
	} else if(request.url == '/profile' && request.method == 'GET') {
		response.writeHead(200, {'Content-Type': 'application/json'})
		response.end('Welcome to Your Profile.')
	} else if(request.url == '/courses' && request.method == 'GET') {
		response.writeHead(200, {'Content-Type': 'application/json'})
		response.write("Here's our Courses Available.")
		response.write(JSON.stringify(courses))
		response.end()
	} else if(request.url == '/addCourse' && request.method == 'POST') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Add Course to our Resources');

	} else if(request.url = "/updateCourse" && request.method == "PUT"){

				response.writeHead(200, {'Content-Type': 'text/plain'});
				response.end('Update a course to our resources');

	} else if(request.url = "/archiveCourses" && request.method == "DELETE"){

					response.writeHead(200, {'Content-Type': 'text/plain'});
					response.end('Archive courses to our resources');

	}

});

server.listen(port);

console.log(`Server is running on localhost: ${port}`);
