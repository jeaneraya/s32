const http = require('http');

const port = 4000;

const server = http.createServer(function(request,response){
	if(request.url == '/items' && request.method == 'GET'){
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end('Data is retrieved from the Database');
	}else if(request.url == '/items' && request.method == 'POST'){
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end('Data is sent to the Database');
	}
});

server.listen(port);

console.log(`Server is now accessible at localhost: ${port}.`);